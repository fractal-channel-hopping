/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011,2015,2019 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _DEFAULT_SOURCE

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include "channel.h"

void *channel_main(void *arg) {
  struct channel *channel = arg;
  if (channel->width == 256 && channel->height == 256) {
    int bytes = 256 * 256 + 128 * 128 * 2;
    const char *fmt = "mplayer -quiet -really-quiet -loop 0 -shuffle -aspect 1/1 -vf scale=256:256 -ao 'jack:name=%s:port=fch.*%s_[1-2]' -vo 'yuv4mpeg:file=tmp/%s.fifo' -fixed-vo '%s/'*.mp4 >'tmp/%s.mplayer.log' 2>&1 & cat 'tmp/%s.fifo'";
    int cmdlen = strlen(fmt) + 6 * strlen(channel->name) + 64;
    char *cmd = malloc(cmdlen);
    snprintf(cmd, cmdlen - 2, fmt, channel->name, channel->name, channel->name, channel->name, channel->name, channel->name);
    cmd[cmdlen-1] = 0;
    FILE *video;
    if ((video = popen(cmd, "r"))) {
      const char *vhdr25 = "YUV4MPEG2 W256 H256 F25:1 Ip A1:1\n";
      const char *vhdr50 = "YUV4MPEG2 W256 H256 F50:1 Ip A1:1\n";
      const char *fhdr = "FRAME\n";
      char hdr[64];
      if (1 != fread(hdr, strlen(vhdr25), 1, video)) { channel->aborted = 1; goto cleanup; }
      hdr[strlen(vhdr25)] = 0;
      if (strcmp(vhdr25, hdr) && strcmp(vhdr50, hdr)) { channel->aborted = 2; goto cleanup; }
      while (! channel->quit) {
        if (1 != fread(hdr, strlen(fhdr), 1, video)) { channel->aborted = 3; break; }
        hdr[strlen(fhdr)] = 0;
        if (strcmp(fhdr, hdr)) { channel->aborted = 4; break; }
        if (1 != fread(channel->image, bytes, 1, video)) { channel->aborted = 5; break; }
      }
      cleanup:
      pclose(video);
    }
    free(cmd);
  }
  if (channel->aborted) {
    fprintf(stderr, "channel '%s' aborted '%d'\n", channel->name, channel->aborted);
  }
  pthread_exit(0);
  return 0;
}

struct channel *channel_start(const char *name) {
  struct channel *channel = calloc(1, sizeof(struct channel));
  // clear YUV to black
  memset(&channel->image[0], 0, VIDEO_WIDTH * VIDEO_HEIGHT);
  memset(&channel->image[VIDEO_WIDTH * VIDEO_HEIGHT], 128, 2 * VIDEO_WIDTH/2 * VIDEO_HEIGHT/2);
  channel->name   = strdup(name);
  channel->width  = VIDEO_WIDTH;
  channel->height = VIDEO_HEIGHT;
  if (pthread_create(&channel->thread, 0, channel_main, channel)) {
    free(channel->name);
    free(channel);
    return 0;
  }
  return channel;
}

int channel_stop(struct channel *channel) {
  channel->quit = 1;
  pthread_join(channel->thread, 0);
  int r = channel->aborted;
  free(channel->name);
  free(channel);
  return r;
}
