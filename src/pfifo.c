/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <assert.h>
#include <string.h>
#include "pfifo.h"

void *pfifo_consumerthread(void *);

struct pfifo *pfifo_create(pfifo_consumer *consumer, void *consumerdata) {
  struct pfifo *p = malloc(sizeof(struct pfifo));
  if (!p) return 0;
  list_init(&(p->list));
  p->running = 1;
  p->consumer = consumer;
  p->consumerdata = consumerdata;
  p->mutex = malloc(sizeof(pthread_mutex_t));
  pthread_mutex_init(p->mutex, 0);
  p->nonempty = malloc(sizeof(pthread_cond_t));
  pthread_cond_init(p->nonempty, 0);
  pthread_create(&(p->thread), 0, pfifo_consumerthread, p);
  return p;
}

void pfifo_destroy(struct pfifo *p) {
  p->running = 0;
  pthread_join(p->thread, 0);
  // FIXME proper cleanup...
}

void pfifo_enqueue(struct pfifo *p, size_t length, const void *data) {
  struct pfifo_node *n = malloc(sizeof(struct pfifo_node));
  assert(n);
  n->length = length;
  n->data = malloc(length);
  assert(n->data);
  memcpy(n->data, data, length);
  pthread_mutex_lock(p->mutex);
  list_inserttail(&(p->list), &(n->node));
  pthread_mutex_unlock(p->mutex);
  pthread_cond_signal(p->nonempty);
}

void *pfifo_consumerthread(void *fifo) {
  struct pfifo *p = fifo;
  pthread_mutex_lock(p->mutex);
  while (p->running) {
    while (list_isempty(&(p->list))) pthread_cond_wait(p->nonempty, p->mutex);
    struct pfifo_node *n = (struct pfifo_node *) list_removehead(&(p->list));
    p->consumer(p->consumerdata, n->length, n->data);
    free(n->data);
    free(n);
  }
  pthread_exit(0);
  return 0;
}
