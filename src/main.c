/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011,2019 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _DEFAULT_SOURCE
#define _POSIX_C_SOURCE 200809

#include <assert.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <GL/glew.h>
#include <GL/glut.h>

#include <jack/jack.h>

#include "channel.h"
#include "record.h"
#include "fch.frag.c"
#include "yuv2rgb.frag.c"

int max(int x, int y) {
  return x > y ? x : y;
}

unsigned int roundtwo(unsigned int x) {
  assert(x <= 1u << 31u); // termination condition
  unsigned int y = 1;
  while (y < x) y <<= 1;
  return y;
}

unsigned int logtwo(unsigned int x) {
  assert(x <= 1u << 31u); // termination condition
  unsigned int y = 1, z = 0;
  while (y < x) { y <<= 1; z += 1; };
  return z;
}

int focus = 0;
int speed = 75;
int frames = 0;
int tframes = 0;

int count;
struct channel **channels;

struct record *recorder = 0;

int winw, winh;
int tsize;
GLuint texiny, texinu, texinv, timages, toutput, tmatch[3]; // hardcoded: (12) channels / 4
GLuint fbo;

// fractalization shader
GLhandleARB prog;
GLhandleARB frag;
GLint uimages, umatches, ufocus, ublend0, ublend1, ublend2;
GLfloat vmatches[4 * 4][3];

// yuv2rgb shader
GLhandleARB prog_yuv2rgb;
GLhandleARB frag_yuv2rgb;
GLint yuv2rgb_y, yuv2rgb_u, yuv2rgb_v;

double zoom;
int zoomi;
int zoomj;
double blend0;
double blend1;
double blend2;

jack_client_t *jclient;
jack_port_t *jporto[2];
jack_port_t *jport[CHANNEL_COUNT_MAX][2];
float jlevel[2][CHANNEL_COUNT_MAX];
int jwhich = 0;

void errorcb(const char *desc) {
  fprintf(stderr, "JACK error: %s\n", desc);
}

void shutdowncb(void *arg) {
  exit(1);
}

int processcb(jack_nframes_t nframes, void *arg) {
  jack_default_audio_sample_t *in[CHANNEL_COUNT_MAX][2], *out[2];
  for (int c = 0; c < count; ++c) {
    for (int k = 0; k < 2; ++k) {
      in[c][k] = (jack_default_audio_sample_t *) jack_port_get_buffer(jport[c][k], nframes);
    }
  }
  out[0] = (jack_default_audio_sample_t *) jack_port_get_buffer(jporto[0], nframes);
  out[1] = (jack_default_audio_sample_t *) jack_port_get_buffer(jporto[1], nframes);
  for (jack_nframes_t i = 0; i < nframes; ++i) {
    for (int k = 0; k < 2; ++k) {
      jack_default_audio_sample_t o = 0;
      for (int c = 0; c < count; ++c) {
        o += jlevel[jwhich][c] * in[c][k][i];
      }
      out[k][i] = o;
    }
  }
  return 0;
}

void realloctexture(GLuint t, GLenum fmt, float r, float g, float b) {
  // resize texture
  glBindTexture(GL_TEXTURE_2D, t);
  glTexImage2D(GL_TEXTURE_2D, 0, fmt, tsize, tsize, 0, fmt, GL_UNSIGNED_BYTE, 0);
  glBindTexture(GL_TEXTURE_2D, 0);
  // clear texture
  glViewport(0, 0, tsize, tsize);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, t, 0);
  GLenum dbs[] = { GL_COLOR_ATTACHMENT0_EXT };
  glDrawBuffers(1, dbs);
  glClearColor(r, g, b, 0);
  glClear(GL_COLOR_BUFFER_BIT);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}

void reshapecb(int w, int h) {
  if (recorder && (winw != w || winh != h)) {
    record_stop(recorder);
    recorder = 0;
  }
  winw = w;
  winh = h;
  int oldtsize = tsize;
  tsize = roundtwo(max(w, h));
  if (oldtsize != tsize) {
    realloctexture(toutput,   GL_RGBA, 0, 0, 0);
    realloctexture(tmatch[0], GL_RGBA, 0, 0, 0);
    realloctexture(tmatch[1], GL_RGBA, 0, 0, 0);
    realloctexture(tmatch[2], GL_RGBA, 0, 0, 0);
  }
}

void displaycb(void) {
  // upload channels of video to tile sheet
  {
    // upload YUV planes
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texiny);
    for (int c = 0; c < count; ++c) {
      glTexSubImage2D(GL_TEXTURE_2D, 0, (c % 4) * VIDEO_WIDTH, (c / 4) * VIDEO_HEIGHT, VIDEO_WIDTH, VIDEO_HEIGHT, GL_LUMINANCE, GL_UNSIGNED_BYTE, &channels[c]->image[0]);
    }
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, texinu);
    for (int c = 0; c < count; ++c) {
      glTexSubImage2D(GL_TEXTURE_2D, 0, (c % 4) * VIDEO_WIDTH/2, (c / 4) * VIDEO_HEIGHT/2, VIDEO_WIDTH/2, VIDEO_HEIGHT/2, GL_LUMINANCE, GL_UNSIGNED_BYTE, &channels[c]->image[VIDEO_WIDTH * VIDEO_HEIGHT]);
    }
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, texinv);
    for (int c = 0; c < count; ++c) {
      glTexSubImage2D(GL_TEXTURE_2D, 0, (c % 4) * VIDEO_WIDTH/2, (c / 4) * VIDEO_HEIGHT/2, VIDEO_WIDTH/2, VIDEO_HEIGHT/2, GL_LUMINANCE, GL_UNSIGNED_BYTE, &channels[c]->image[VIDEO_WIDTH * VIDEO_HEIGHT + VIDEO_WIDTH/2 * VIDEO_HEIGHT/2]);
    }
    // convert to RGB
    glViewport(0, 0, 1024, 1024);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 1, 0, 1, -1, 1);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, timages, 0);
    GLenum dbs[] = { GL_COLOR_ATTACHMENT0_EXT };
    glDrawBuffers(1, dbs);
    glUseProgramObjectARB(prog_yuv2rgb);
    glUniform1i(yuv2rgb_y, 0);
    glUniform1i(yuv2rgb_u, 1);
    glUniform1i(yuv2rgb_v, 2);
    glBegin(GL_QUADS); {
      glTexCoord2f(0, 0); glVertex2f(0, 0);
      glTexCoord2f(1, 0); glVertex2f(1, 0);
      glTexCoord2f(1, 1); glVertex2f(1, 1);
      glTexCoord2f(0, 1); glVertex2f(0, 1);
    } glEnd();
    glUseProgramObjectARB(0);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, timages);
    glGenerateMipmap(GL_TEXTURE_2D);
    glGetTexImage(GL_TEXTURE_2D, 8 /* hardcoded log2(4*256) - 2 */, GL_RGB, GL_FLOAT, vmatches);
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  // set up zooming view
  glViewport(0, 0, winw, winh);
  {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, GRID_WIDTH, 0, GRID_HEIGHT, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    double dx =               zoomi * GRID_WIDTH  / (GRID_WIDTH  - 1.0);
    double dy = GRID_HEIGHT - zoomj * GRID_HEIGHT / (GRID_HEIGHT - 1.0);
    glTranslatef( dx,  dy, 0);
    glScalef(zoom, zoom, zoom);
    glTranslatef(-dx, -dy, 0);
  }

  // mega shader action
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, toutput,   0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, tmatch[0], 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT2_EXT, GL_TEXTURE_2D, tmatch[1], 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT3_EXT, GL_TEXTURE_2D, tmatch[2], 0);
  GLenum dbs[] = { GL_COLOR_ATTACHMENT0_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_COLOR_ATTACHMENT2_EXT, GL_COLOR_ATTACHMENT3_EXT };
  glDrawBuffers(4, dbs);
  glUseProgramObjectARB(prog);
  glBindTexture(GL_TEXTURE_2D, timages);
  glUniform1i(uimages, 0);
  glUniform3fv(umatches, 12, &vmatches[0][0]);
  glUniform1i(ufocus, focus);
  glUniform1f(ublend0, blend0);
  glUniform1f(ublend1, blend1);
  glUniform1f(ublend2, blend2);
  glBegin(GL_QUADS); {
    glTexCoord2f(0, 1); glVertex2f(0         , 0          );
    glTexCoord2f(1, 1); glVertex2f(GRID_WIDTH, 0          );
    glTexCoord2f(1, 0); glVertex2f(GRID_WIDTH, GRID_HEIGHT);
    glTexCoord2f(0, 0); glVertex2f(0         , GRID_HEIGHT);
  } glEnd();
  glBindTexture(GL_TEXTURE_2D, 0);
  glUseProgramObjectARB(0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_2D, 0, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT2_EXT, GL_TEXTURE_2D, 0, 0);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT3_EXT, GL_TEXTURE_2D, 0, 0);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

  // show output image
  glLoadIdentity();
  glBindTexture(GL_TEXTURE_2D, toutput);
  glBegin(GL_QUADS); {
    float tx = winw * 1.0f / tsize;
    float ty = winh * 1.0f / tsize;
    glTexCoord2f(0,  0);  glVertex2f(0         , 0          );
    glTexCoord2f(tx, 0);  glVertex2f(GRID_WIDTH, 0          );
    glTexCoord2f(tx, ty); glVertex2f(GRID_WIDTH, GRID_HEIGHT);
    glTexCoord2f(0,  ty); glVertex2f(0         , GRID_HEIGHT);
  } glEnd();
  glBindTexture(GL_TEXTURE_2D, 0);

  // grab auxiliary images to audio mixer
  glBindTexture(GL_TEXTURE_2D, tmatch[0]);
  glGenerateMipmap(GL_TEXTURE_2D);
  glGetTexImage(GL_TEXTURE_2D, logtwo(tsize), GL_RGBA, GL_FLOAT, &jlevel[1 - jwhich][0]);
  glBindTexture(GL_TEXTURE_2D, tmatch[1]);
  glGenerateMipmap(GL_TEXTURE_2D);
  glGetTexImage(GL_TEXTURE_2D, logtwo(tsize), GL_RGBA, GL_FLOAT, &jlevel[1 - jwhich][4]);
  glBindTexture(GL_TEXTURE_2D, tmatch[2]);
  glGenerateMipmap(GL_TEXTURE_2D);
  glGetTexImage(GL_TEXTURE_2D, logtwo(tsize), GL_RGBA, GL_FLOAT, &jlevel[1 - jwhich][8]);
  glBindTexture(GL_TEXTURE_2D, 0);
  double s = 0;
  for (int c = 0; c < count; ++c) {
    s += jlevel[1 - jwhich][c];
  }
  if (! s) {
    s = 1;
  }
  for (int c = 0; c < count; ++c) {
    jlevel[1 - jwhich][c] /= s;
  }
  jwhich = 1 - jwhich;

  // PPM recording on stdout
  if (recorder) {
    record_frame(recorder);
  }

  glutSwapBuffers();

  // bug free code?
  glutReportErrors();

  // we done a frame
  tframes++;
  frames++;
}

float smatch[4][16][4][16][3];
float matches[4][4][3];

struct timespec clock0;
struct timespec clock1;

void timercb(int v) {
  glutTimerFunc(1, timercb, v);
  clock_gettime(CLOCK_REALTIME, &clock1);
  double dt = (clock1.tv_sec - clock0.tv_sec) + 1.0e-9 * (clock1.tv_nsec - clock0.tv_nsec);
  if (dt < 0.04 * tframes) {
    return;
  }
  if (frames == speed) {
    // refocus
    glBindTexture(GL_TEXTURE_2D, timages);
    glGenerateMipmap(GL_TEXTURE_2D);
    glGetTexImage(GL_TEXTURE_2D, 4, GL_RGB, GL_FLOAT, &smatch[0][0][0][0][0]);
    glGetTexImage(GL_TEXTURE_2D, 8, GL_RGB, GL_FLOAT, &matches[0][0][0]);
    glBindTexture(GL_TEXTURE_2D, 0);
    float ml = 65536.0;
    int   mi = focus;
    for (int c = 0; c < count; ++c) {
      float s = 0;
      for (int k = 0; k < 3; ++k) {
        float ds = smatch[focus / 4][zoomj][focus % 4][zoomi][k] - vmatches[c][k];
        s += ds * ds;
      }
      if (s < ml) {
        ml = s;
        mi = c;
      }
    }
    focus = mi;
    ml = 65536.0;
    for (int c = 0; c < count; ++c) {
      float s = 0;
      for (int k = 0; k < 3; ++k) {
        float ds = matches[focus / 4][focus % 4][k] - vmatches[c][k];
        s += ds * ds;
      }
      if (s < ml) {
        ml = s;
        mi = c;
      }
    }
    focus = mi;
    // self-centering random walk
    if (rand() % 8) {
      int coin = rand() % (GRID_WIDTH - 1);
      zoomi += coin < zoomi ? -1 : 1;
    }
    if (rand() % 8) {
      int coin = rand() % (GRID_HEIGHT - 1);
      zoomj += coin < zoomj ? -1 : 1;
    }
    frames = 0;
  }

  // zoom blending
  double k = frames * 1.0 / speed;
  zoom = pow(16, k); // hardcoded power - grid size...
  blend2 = 1 - cos(2 * 3.141592653 * (k + 0) / 3);
  blend1 = 1 - cos(2 * 3.141592653 * (k + 1) / 3);
  blend0 = 1 - cos(2 * 3.141592653 * (k + 2) / 3);
  double blendt = blend0 + blend1 + blend2;
  blend0 /= blendt;
  blend1 /= blendt;
  blend2 /= blendt;

  glutPostRedisplay();
}

int fullscreen = 0;

void keyboardcb(unsigned char key, int x, int y) {
  switch (key) {
  case 'R':
    if (recorder) {
      record_stop(recorder);
      recorder = 0;
    } else {
      recorder = record_start(winw, winh, jclient, jporto);
    }
    break;
  case 'Q':
  case 27:
    exit(0);
    break;
  }
}

void keyspecialcb(int key, int x, int y) {
  switch (key) {
  case GLUT_KEY_F11:
    fullscreen = !fullscreen;
    if (fullscreen) {
      glutFullScreen();
      glutSetCursor(GLUT_CURSOR_NONE);
    } else {
      glutReshapeWindow(OUTPUT_WIDTH, OUTPUT_HEIGHT);
      glutSetCursor(GLUT_CURSOR_INHERIT);
    }
    break;
  }
}

void exitcb(void) {
  if (recorder) {
    record_stop(recorder);
    recorder = 0;
  }
  jack_client_close(jclient);
  killpg(getpgrp(), SIGKILL); // kill all our processes
  for (int c = 0; c < count; ++c) {
    channel_stop(channels[c]);
  }
  free(channels);
}

int main(int argc, char **argv) {

  // initialisation
  if (argc <= 1) { return 1; }
  count = argc - 1;
  channels = calloc(count, sizeof(struct channel *));

  srand(time(0));
  zoomj = rand() % GRID_HEIGHT;
  zoomi = rand() % GRID_WIDTH;
  glutInitWindowSize(OUTPUT_WIDTH, OUTPUT_HEIGHT);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("fractal-channel-hopping");
  glewInit();

  // set up jack first

  jack_set_error_function(errorcb);
  if (!(jclient = jack_client_open("fch", 0, 0))) {
    fprintf (stderr, "jack server not running?\n");
    return 1;
  }
  jack_set_process_callback(jclient, processcb, 0);
  jack_on_shutdown(jclient, shutdowncb, 0);
  for (int c = 0; c < count; ++c) {
    for (int k = 0; k < 2; ++k) {
      char namebuf[64];
      snprintf(namebuf, 62, "%s_%d", argv[c + 1], k + 1);
      namebuf[63] = 0;
      jport[c][k] = jack_port_register(jclient, namebuf, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
    }
  }
  for (int k = 0; k < 2; ++k) {
    char namebuf[64];
    snprintf(namebuf, 62, "output_%d", k + 1);
    namebuf[63] = 0;
    jporto[k] = jack_port_register(jclient, namebuf, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
  }
  if (jack_activate(jclient)) {
    fprintf (stderr, "cannot activate JACK client");
    return 1;
  }
  jack_connect(jclient, "fch:output_1", "system:playback_1");
  jack_connect(jclient, "fch:output_2", "system:playback_2");
  
  // then start streaming channels which connect to us via jack

  for (int c = 0; c < count; ++c) {
    channels[c] = channel_start(argv[c + 1]);
  }

  // and prepare the display

  glHint(GL_GENERATE_MIPMAP_HINT, GL_FASTEST);
  glEnable(GL_TEXTURE_2D);
  glGenFramebuffersEXT(1, &fbo);

  // YUV planar video
  tsize = 1024;
  glGenTextures(1, &texiny);
  realloctexture(texiny, GL_LUMINANCE, 0, 0, 0);
  glBindTexture(GL_TEXTURE_2D, texiny);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, 0);
  tsize /= 2;
  glGenTextures(1, &texinu);
  realloctexture(texinu, GL_LUMINANCE, 0.5, 0.5, 0.5);
  glBindTexture(GL_TEXTURE_2D, texinu);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, 0);
  glGenTextures(1, &texinv);
  realloctexture(texinv, GL_LUMINANCE, 0.5, 0.5, 0.5);
  glBindTexture(GL_TEXTURE_2D, texinv);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, 0);

  // RGB interleaved video
  tsize = 1024;
  glGenTextures(1, &timages);
  realloctexture(timages, GL_RGBA, 0, 0, 0);
  glBindTexture(GL_TEXTURE_2D, timages);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glBindTexture(GL_TEXTURE_2D, 0);

  // RGB output
  tsize = roundtwo(max(OUTPUT_WIDTH, OUTPUT_HEIGHT));
  glGenTextures(1, &toutput);
  realloctexture(toutput, GL_RGBA, 0, 0, 0);
  glBindTexture(GL_TEXTURE_2D, toutput);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glBindTexture(GL_TEXTURE_2D, 0);
  glGenTextures(3, &tmatch[0]);
  realloctexture(tmatch[0], GL_RGBA, 0, 0, 0);
  realloctexture(tmatch[1], GL_RGBA, 0, 0, 0);
  realloctexture(tmatch[2], GL_RGBA, 0, 0, 0);

  // fractalization shader
  GLint success;
  prog = glCreateProgramObjectARB();
  frag = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
  glShaderSourceARB(frag, 1, &fch_frag, 0);
  glCompileShaderARB(frag);
  glAttachObjectARB(prog, frag);
  glLinkProgramARB(prog);
  glGetObjectParameterivARB(prog, GL_OBJECT_LINK_STATUS_ARB, &success);
  if (! success) {
    GLhandleARB obj = prog;
    int infologLength = 0;
    int maxLength;
    if (glIsShader(obj)) {
      glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &maxLength);
    } else {
      glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &maxLength);
    }
    char *infoLog = malloc(maxLength);
    if (!infoLog) {
      exit(1);
    }
    if (glIsShader(obj)) {
      glGetShaderInfoLog(obj, maxLength, &infologLength, infoLog);
    } else {
      glGetProgramInfoLog(obj, maxLength, &infologLength, infoLog);
    }
    if (infologLength > 0) {
      fprintf(stderr, "%s\n", infoLog);
    }
    free(infoLog);
    exit(1);
  }
  uimages  = glGetUniformLocationARB(prog, "images");
  umatches = glGetUniformLocationARB(prog, "matches");
  ufocus   = glGetUniformLocationARB(prog, "focus");
  ublend0  = glGetUniformLocationARB(prog, "blend0");
  ublend1  = glGetUniformLocationARB(prog, "blend1");
  ublend2  = glGetUniformLocationARB(prog, "blend2");

  // YUV2RGB shader
  prog_yuv2rgb = glCreateProgramObjectARB();
  frag_yuv2rgb = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
  glShaderSourceARB(frag_yuv2rgb, 1, &yuv2rgb_frag, 0);
  glCompileShaderARB(frag_yuv2rgb);
  glAttachObjectARB(prog_yuv2rgb, frag_yuv2rgb);
  glLinkProgramARB(prog_yuv2rgb);
  glGetObjectParameterivARB(prog_yuv2rgb, GL_OBJECT_LINK_STATUS_ARB, &success);
  if (! success) {
    GLhandleARB obj = prog_yuv2rgb;
    int infologLength = 0;
    int maxLength;
    if (glIsShader(obj)) {
      glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &maxLength);
    } else {
      glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &maxLength);
    }
    char *infoLog = malloc(maxLength);
    if (!infoLog) {
      exit(1);
    }
    if (glIsShader(obj)) {
      glGetShaderInfoLog(obj, maxLength, &infologLength, infoLog);
    } else {
      glGetProgramInfoLog(obj, maxLength, &infologLength, infoLog);
    }
    if (infologLength > 0) {
      fprintf(stderr, "%s\n", infoLog);
    }
    free(infoLog);
    exit(1);
  }
  yuv2rgb_y = glGetUniformLocationARB(prog_yuv2rgb, "y");
  yuv2rgb_u = glGetUniformLocationARB(prog_yuv2rgb, "u");
  yuv2rgb_v = glGetUniformLocationARB(prog_yuv2rgb, "v");

  // callbacks
  glutKeyboardFunc(keyboardcb);
  glutSpecialFunc(keyspecialcb);
  glutReshapeFunc(reshapecb);
  glutDisplayFunc(displaycb);
  glutTimerFunc(1, timercb, 1);
  atexit(exitcb);

  // main loop
  clock_gettime(CLOCK_REALTIME, &clock0);
  glutMainLoop();
  return 0;
}
