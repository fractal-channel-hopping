/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CHANNEL_H
#define CHANNEL_H 1

#include <pthread.h>

#include "config.h"

struct channel {
  pthread_t thread;
  char *name;
  int quit;
  int aborted;
  int width;
  int height;
  unsigned char image[VIDEO_WIDTH * VIDEO_HEIGHT + 2 * VIDEO_WIDTH/2 * VIDEO_HEIGHT/2];
};

struct channel *channel_start(const char *name);
int channel_stop(struct channel *channel);

#endif
