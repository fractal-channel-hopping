/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#version 130

uniform sampler2D y;
uniform sampler2D u;
uniform sampler2D v;

void main(void) {
  const mat4 m = mat4(
    1.1643828125,  0.0,            1.59602734375, -0.87078515625,
    1.1643828125, -0.39176171875, -0.81296875,     0.52959375,
    1.1643828125,  2.017234375,    0.0,           -1.081390625,
    0.0,           0.0,            0.0,            1.0
  );
  vec2 p = gl_TexCoord[0].xy;
  vec3 yuv = vec3(texture(y, p).r, texture(u, p).r, texture(v, p).r);
  gl_FragData[0] = vec4(yuv, 1.0) * m;
}
