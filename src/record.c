/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011,2019 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define _DEFAULT_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <GL/glew.h>
#include <jack/jack.h>

#include "pfifo.h"
#include "record.h"

void record_writer(void *arg, size_t l, void *data) {
  struct record *record = arg;
  unsigned char *buffer = data;
  fwrite(record->header, strlen(record->header), 1, record->ppm);
  for (int y = record->height - 1; y >= 0; --y) {
    fwrite(buffer + record->width * y * 3, record->width * 3, 1, record->ppm);
  }
  if (! jack_port_connected_to(record->jport[0], "record:input_1")) {
    jack_connect(record->jclient, jack_port_name(record->jport[0]), "record:input_1");
  }
  if (! jack_port_connected_to(record->jport[1], "record:input_2")) {
    jack_connect(record->jclient, jack_port_name(record->jport[1]), "record:input_2");
	}
}

struct record *record_start(int w, int h, jack_client_t *jclient, jack_port_t **jports) {
  struct record *record = malloc(sizeof(struct record));
  record->width = w;
  record->height = h;
  record->bytes = w * h * 3;
  record->buffer = malloc(record->bytes);
  snprintf(record->header, 62, "P6\n%d %d 255\n", w, h);
  record->header[63] = 0;
  const char *vfmt = "ppmtoy4m -v0 -S444 -F25:1 2>'tmp/record.ppm.log' | ffmpeg -loglevel 0 -f yuv4mpegpipe -i pipe:- -ac 2 -f jack -i record -target pal-dvd -shortest 'fractal-channel-hopping_%Y-%m-%d_%H-%M-%S_%z.mpeg' >'tmp/record.ffmpeg.log' 2>&1";
  int vlen = strlen(vfmt) + 64;
  char *vcmd = malloc(vlen);
  time_t t = time(NULL);
  struct tm tm;
  localtime_r(&t, &tm);
  if (0 == strftime(vcmd, vlen, vfmt, &tm)) {
    return 0;
  }
  record->jclient = jclient;
  record->jport[0] = jports[0];
  record->jport[1] = jports[1];
  record->ppm = popen(vcmd, "w");
  record->pfifo = pfifo_create(record_writer, record);
  return record;
}

void record_frame(struct record *record) {
  glReadPixels(0, 0, record->width, record->height, GL_RGB, GL_UNSIGNED_BYTE, record->buffer);
  pfifo_enqueue(record->pfifo, record->bytes, record->buffer);
}

void record_stop(struct record *record) {
  pfifo_destroy(record->pfifo);
  pclose(record->ppm);
  free(record->buffer);
  free(record);
}
