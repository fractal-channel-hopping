/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011,2019 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#version 130

uniform sampler2D images; // 4x4 tile sheet
uniform vec3 matches[12]; // hardcoded maximum channels
uniform int focus;
uniform float blend0;
uniform float blend1;
uniform float blend2;

vec3 get(int n, vec2 p, float bias, bool wrap) {
  int j = int(floor(float(n) / 4.0));
  int i = n - 4 * j;
  vec2 p1 = p;
  if (wrap) {
    float k = pow(2.0, 8.0 - bias);
    p1 = floor(p1 * k) / k;
  }
  p1 *= 254.0 / 256.0;
  p1 += 1.0 / 256.0;
  vec2 q = (vec2(float(i), float(j)) + p1) / 4.0;
  return textureLod(images, q, bias).rgb;
}

int match(int def, vec3 c) {
  float d = 1024.0;
  int m = def;
  for (int i = 0; i < 12; ++i) {
    if (length(matches[i]) > 0.0) {
      float d2 = length(c - matches[i]);
      if (d2 < d) {
        d = d2;
        m = i;
      }
    }
  }
  return m;
}

void main(void) {
  int  f0 = focus;
  vec2 p0 = gl_TexCoord[0].xy;
  vec3 c0 = get(f0, p0, 0.0, false);
  vec3 ca = get(f0, p0, 4.0, true);

  int  f1 = match(f0, ca);
  vec2 p1 = 16.0 * p0; p1 -= floor(p1);
  vec3 c1 = get(f1, p1, 0.0, false);
  vec3 cb = get(f1, p1, 4.0, true);

  int  f2 = match(f1, cb);
  vec2 p2 = 16.0 * p1; p2 -= floor(p2);
  vec3 c2 = get(f2, p2, 0.0, false);

  float channel[12];
  channel[0] = 0.0;
  channel[1] = 0.0;
  channel[2] = 0.0;
  channel[3] = 0.0;
  channel[4] = 0.0;
  channel[5] = 0.0;
  channel[6] = 0.0;
  channel[7] = 0.0;
  channel[8] = 0.0;
  channel[9] = 0.0;
  channel[10] = 0.0;
  channel[11] = 0.0;
  channel[f0] += blend0;
  channel[f1] += blend1;
  channel[f2] += blend2;
  gl_FragData[0] = vec4(blend0 * c0 + blend1 * c1 + blend2 * c2, 1.0);
  gl_FragData[1] = vec4(channel[0], channel[1], channel[2], channel[3]);
  gl_FragData[2] = vec4(channel[4], channel[5], channel[6], channel[7]);
  gl_FragData[3] = vec4(channel[8], channel[9], channel[10], channel[11]);
}
