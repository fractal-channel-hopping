/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RECORD_H
#define RECORD_H 1

#include <stdio.h>

#include <jack/jack.h>

#include "pfifo.h"

struct record {
  int width;
  int height;
  int bytes;
  unsigned char *buffer;
  char header[64];
  FILE *ppm;
  FILE *wav;
  struct pfifo *pfifo;
  jack_client_t *jclient;
  jack_port_t *jport[2];
};

struct record *record_start(int w, int h, jack_client_t *jclient, jack_port_t **jports);
void record_frame(struct record *record);
void record_stop(struct record *record);

#endif
