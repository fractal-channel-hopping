/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONFIG_H
#define CONFIG_H 1

#define VIDEO_WIDTH  256
#define VIDEO_HEIGHT 256
#define GRID_WIDTH    16
#define GRID_HEIGHT   16

//#define TEXTURE_SIZE 256
#define TEXTURE_X      1.0f
#define TEXTURE_Y      0.5625f

#define CHANNEL_COUNT_MAX 16

#define OUTPUT_WIDTH  1024
#define OUTPUT_HEIGHT 576

#endif
