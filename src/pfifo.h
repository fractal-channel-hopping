/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PFIFO_H
#define PFIFO_H 1

#include <stdlib.h>
#include <pthread.h>
#include "list.h"

typedef void (pfifo_consumer)(void *, size_t, void *);

struct pfifo_node {
  struct node node;
  size_t length;
  void *data;
};

struct pfifo {
  pfifo_consumer *consumer;
  void *consumerdata;
  struct list list;
  pthread_t thread;
  pthread_mutex_t *mutex;
  pthread_cond_t *nonempty;
  int running;
};

struct pfifo *pfifo_create(pfifo_consumer *consumer, void *consumerdata);
void pfifo_destroy(struct pfifo *fifo);
void pfifo_enqueue(struct pfifo *fifo, size_t length, const void *data);

#endif
