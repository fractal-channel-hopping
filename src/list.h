/*
  fractal-channel-hopping -- infinite fractal television zoom
  Copyright (C) 2011 Claude Heiland-Allen

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIST_H
#define LIST_H 1

struct node {
  struct node *next;
  struct node *pred;
};

struct list {
  struct node *head;
  struct node *tail;
  // private
  struct node headNode;
  struct node tailNode;
};

void list_init(struct list *l);
int list_ishead(struct node *n);
int list_istail(struct node *n);
int list_isempty(struct list *l);
int list_length(struct list *l);
void list_remove(struct node *n);
void list_insertbefore(struct node *n, struct node *beforethis);
void list_insertafter(struct node *n, struct node *afterthis);
void list_inserttail(struct list *l, struct node *n);
struct node *list_removehead(struct list *l);

#endif
