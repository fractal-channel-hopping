#!/bin/sh
for i in *.jpeg *.png
do
  convert "${i}" -geometry 1920x1080^ -gravity center                                       -extent 1920x1080 -blur 0x32 "/tmp/background.png"
  convert "${i}" -geometry 1920x1080  -gravity center -alpha opaque -background transparent -extent 1920x1080            "/tmp/foreground.png"
  composite -compose Over -gravity center "/tmp/foreground.png" "/tmp/background.png" "${i}.ppm"
done
for i in *.ppm
do
  cat "${i}" | tail -c $((1920 * 1080 * 3))
done > image.data
for i in *.ppm
do
  convert "${i}" -geometry '16x16!' "/tmp/thumb.ppm"
  cat "/tmp/thumb.ppm" | tail -c $((16 * 16 * 3))
done > thumbs.data
