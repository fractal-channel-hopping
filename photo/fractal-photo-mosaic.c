#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define COUNT (103+1)
#define WIDTH 1920
#define HEIGHT 1080

static unsigned char raw[COUNT][HEIGHT][WIDTH][3];
static unsigned char raw16[COUNT][16][16][3];

static double lab16[COUNT][16][16][3];
static double lab2[COUNT][2][2][3];

static float graph[COUNT][8][8];

static int visited[COUNT];

static unsigned char out[HEIGHT][WIDTH][3];

static const char *vert =
"#version 400 core\n"
"uniform vec2 delta;\n"
"uniform float zoom;\n"
"layout(location = 0) in vec2 pos;\n"
"layout(location = 1) in vec2 tc;\n"
"out vec2 c;\n"
"void main() {\n"
"  gl_Position = vec4(zoom * (pos - delta) + delta, 0.0, 1.0);\n"
"  c = vec2(tc.x, 1.0 - tc.y);\n"
"}\n"
;

static const char *frag =
"#version 400 core\n"
"uniform sampler2DArray tex;\n"
"uniform sampler2DArray map;\n"
"uniform int ix;\n"
"uniform vec3 blend;\n"
"in vec2 c;\n"
"layout(location = 0) out vec3 colour;\n"
"void main() {\n"
"  float l = textureQueryLod(tex, c).y;\n"
"  vec3 p0 = vec3(c, float(ix));\n"
"  vec3 p1 = vec3(8.0 * p0.xy, texture(map, p0).x);\n"
"  p1.xy -= floor(p1.xy);\n"
"  vec3 p2 = vec3(8.0 * p1.xy, texture(map, p1).x);\n"
"  p2.xy -= floor(p2.xy);\n"
"  vec3 sum = vec3(0.0);\n"
"  sum += textureLod(tex, p0, l    ).rgb * blend.x;\n"
"  sum += textureLod(tex, p1, l + 3).rgb * blend.y;\n"
"  sum += textureLod(tex, p2, l + 6).rgb * blend.z;\n"
"  colour = sum;\n"
"}\n"
;

static void debug_program(GLuint program, const char *name) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    }
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, 0, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL shader program info log\n", name);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
    assert(linked == GL_TRUE);
  } else {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

static void debug_shader(GLuint shader, GLenum type, const char *name) {
  const char *tname = 0;
  switch (type) {
    case GL_VERTEX_SHADER:   tname = "vertex";   break;
    case GL_FRAGMENT_SHADER: tname = "fragment"; break;
    case GL_COMPUTE_SHADER:  tname = "compute";  break;
    default:                 tname = "unknown";  break;
  }
  if (shader) {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    }
    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetShaderInfoLog(shader, length, 0, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL %s shader info log\n", name, tname);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
    assert(compiled == GL_TRUE);
  } else {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

static void compile_shader(GLint program, GLenum type, const char *name, const GLchar *source) {
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  debug_shader(shader, type, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

static GLint compile_program(const char *name, const GLchar *vert, const GLchar *frag) {
  GLint program = glCreateProgram();
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , name, vert); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, name, frag); }
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}
static double xyz2lab_f(double t)
{
  static const double e = 0.008856;
  static const double k = 903.3;
  if (t > e)
    return cbrt(t);
  else
    return (k * t + 16) / 116;
}
static void xyz2lab(double x, double y, double z, double *l, double *a, double *b)
{
  static const double xn = 0.95047;
  static const double yn = 1.00000;
  static const double zn = 1.08883;
  x /= xn;
  y /= yn;
  z /= zn;
  x = xyz2lab_f(x);
  y = xyz2lab_f(y);
  z = xyz2lab_f(z);
  *l = 116 * y - 16;
  *a = 500 * (x - y);
  *b = 200 * (y - z);
}

static double srgb2xyz_f(double c)
{
  if (c < 0.04045)
    return c / 12.92;
  else
    return pow((c + 0.055) / 1.055, 2.4);
}
static void srgb2xyz(double r, double g, double b, double *x, double *y, double *z)
{
  static const double m[3][3] =
    { { 0.4124, 0.3576, 0.1805 }
    , { 0.2126, 0.7152, 0.0722 }
    , { 0.0193, 0.1192, 0.9505 }
    };
  r = srgb2xyz_f(r);
  g = srgb2xyz_f(g);
  b = srgb2xyz_f(b);
  *x = m[0][0] * r + m[0][1] * g + m[0][2] * b;
  *y = m[1][0] * r + m[1][1] * g + m[1][2] * b;
  *z = m[2][0] * r + m[2][1] * g + m[2][2] * b;
}


static void srgb2lab(double r, double g, double b, double *l, double *a, double *bb)
{
  double x, y, z;
  srgb2xyz(r, g, b, &x, &y, &z);
  xyz2lab(x, y, z, l, a, bb);
}


extern int main()
{
  srand(0x1cedcafe);

  FILE *fraw = fopen("image.data", "rb");
  fread(&raw[0][0][0][0], (COUNT-1) * WIDTH * HEIGHT * 3, 1, fraw);
  fclose(fraw);

  fraw = fopen("thumbs.data", "rb");
  fread(&raw16[0][0][0][0], (COUNT-1) * 16 * 16 * 3, 1, fraw);
  fclose(fraw);

  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(WIDTH, HEIGHT, "fractalize", 0, 0);
  glfwMakeContextCurrent(window);
  if (! window) {
    fprintf(stderr, "glfw\n");
    return 1;
  }
  glewInit();

  GLuint program = compile_program("shader", vert, frag);
  glUseProgram(program);
  GLint utex = glGetUniformLocation(program, "tex");
  GLint umap = glGetUniformLocation(program, "map");
  GLint uix = glGetUniformLocation(program, "ix");
  GLint uzoom = glGetUniformLocation(program, "zoom");
  GLint udelta = glGetUniformLocation(program, "delta");
  GLint ublend = glGetUniformLocation(program, "blend");
  glUniform1i(utex, 0);
  glUniform1i(umap, 1);

  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  GLfloat vbo_data[] =
    { -1, -1, 0, 0
    , -1,  1, 0, 1
    ,  1, -1, 1, 0
    ,  1,  1, 1, 1
    };
  glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), vbo_data, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), ((char *)0) + 2 * sizeof(GLfloat));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  GLuint tex[2];
  glGenTextures(2, &tex[0]);

  glActiveTexture(GL_TEXTURE0 + 0);
  glBindTexture(GL_TEXTURE_2D_ARRAY, tex[0]);
  glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGB, WIDTH, HEIGHT, COUNT, 0, GL_RGB, GL_UNSIGNED_BYTE, &raw[0][0][0][0]);
  glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  for (int k = 0; k < COUNT - 1; ++k)
  for (int j = 0; j < 16; ++j)
  for (int i = 0; i < 16; ++i)
    srgb2lab(raw16[k][j][i][0]/255.0, raw16[k][j][i][1]/255.0, raw16[k][j][i][2]/255.0, &lab16[k][j][i][0], &lab16[k][j][i][1], &lab16[k][j][i][2]);

  for (int k = 0; k < COUNT - 1; ++k)
  for (int j = 0; j < 2; ++j)
  for (int i = 0; i < 2; ++i)
  {
    double l = 0;
    double a = 0;
    double b = 0;
    for (int jj = 0; jj < 8; ++jj)
    for (int ii = 0; ii < 8; ++ii)
    {
      l += lab16[k][8 * j + jj][8 * i + ii][0];
      a += lab16[k][8 * j + jj][8 * i + ii][1];
      b += lab16[k][8 * j + jj][8 * i + ii][2];
    }
    l /= 8 * 8;
    a /= 8 * 8;
    b /= 8 * 8;
    lab2[k][j][i][0] = l;
    lab2[k][j][i][1] = a;
    lab2[k][j][i][2] = b;
  }

  for (int k = 0; k < COUNT - 1; ++k)
  for (int j = 0; j < 8; ++j)
  for (int i = 0; i < 8; ++i)
  {
    double min_metric = 1.0 / 0.0;
    int min_index = -1;
    for (int kk = 0; kk < COUNT - 1; ++kk)
    {
      if (kk == k) continue;
      double s = 0;
      for (int jj = 0; jj < 2; ++jj)
      for (int ii = 0; ii < 2; ++ii)
      for (int c = 0; c < 3; ++c)
      {
        double x = lab16[k][2*j + jj][2*i + ii][c];
        double y = lab2[kk][jj][ii][c];
        double d = x - y;
        s += d * d;
      }
      if (s < min_metric)
      {
        min_metric = s;
        min_index = kk;
      }
    }
    graph[k][j][i] = min_index;
  }
  for (int j = 0; j < 8; ++j)
  for (int i = 0; i < 8; ++i)
    graph[COUNT - 1][j][i] = 0;

  glActiveTexture(GL_TEXTURE0 + 1);
  glBindTexture(GL_TEXTURE_2D_ARRAY, tex[1]);
  glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, 8, 8, COUNT, 0, GL_RED, GL_FLOAT, &graph[0][0][0]);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  int ix = COUNT-1;
  int zoomi = 4;
  int zoomj = 4;
  int speed = 150;
  double bdx = 0, bdy = 0;
  for (int frame = 1; frame <= 15 * 60 * 60; ++frame)
  {
    if ((frame % speed) == 0)
    {
      ix = graph[ix][zoomj][zoomi];
      int mi = 0x7fffffff;
      for (int j = 0; j < 8; ++j)
      for (int i = 0; i < 8; ++i)
      {
        int k = graph[ix][j][i];
        int m = visited[k];
        mi = m < mi ? m : mi;
      }
      int n = 0;
      for (int j = 0; j < 8; ++j)
      for (int i = 0; i < 8; ++i)
      {
        int k = graph[ix][j][i];
        int m = visited[k];
        n += mi == m;
      }
      int coin = rand() % n;
      n = 0;
      for (int j = 0; j < 8; ++j)
      for (int i = 0; i < 8; ++i)
      {
        int k = graph[ix][j][i];
        int m = visited[k];
        if (mi == m)
        {
          if (coin == n)
          {
            zoomj = j;
            zoomi = i;
            visited[k] += 1;
          }
          n += 1;
        }
      }
    }
    // zoom blending
    double k = ((frame % speed) + 0.5) / speed;
    double zoom = pow(8, k); // hardcoded power - grid size...
    double blend2 = 1 - cos(2 * 3.141592653 * (k + 0) / 3);
    double blend1 = 1 - cos(2 * 3.141592653 * (k + 1) / 3);
    double blend0 = 1 - cos(2 * 3.141592653 * (k + 2) / 3);
    double blendt = blend0 + blend1 + blend2;
    blend0 /= blendt;
    blend1 /= blendt;
    blend2 /= blendt;
    double dx = zoomi * 8 / (8 - 1.0);
    double dy = 8 - zoomj * 8 / (8 - 1.0);
    dx /= 4;
    dy /= 4;
    dx -= 1;
    dy -= 1;
    bdx *= 0.95;
    bdy *= 0.95;
    bdx += 0.05 * dx;
    bdy += 0.05 * dy;
    glUniform1i(uix, ix);
    glUniform1f(uzoom, zoom);
    glUniform2f(udelta, bdx, bdy);
    glUniform3f(ublend, blend0, blend1, blend2);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glfwSwapBuffers(window);
    glReadPixels(0, 0, WIDTH, HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, &out[0][0][0]);
    printf("P6\n%d %d\n255\n", WIDTH, HEIGHT);
    for (int j = HEIGHT - 1; j >= 0; --j)
    {
      fwrite(&out[j][0][0], WIDTH * 3, 1, stdout);
    }
    fflush(stdout);
  }
  return 0;
}
