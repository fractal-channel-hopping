#!/bin/bash
FOV=10
W=64
H=64
for N in $(seq 10000 11023)
do
  Y=$(( RANDOM * 360 / 32767 - 180 ))
  P=$( echo "main = print . (round :: Double -> Int) . (/ pi) . (* 180) . asin . (* 2) . (+ (-0.5)) . (/ 32767) \$ ($RANDOM)" | runhugs /dev/stdin )
  R=$(( RANDOM * 360 / 36767 - 180 ))
  if [[ "$N" = "00" ]]
  then
    Y=0
    P=0
    R=0
  fi
  ffmpeg -hide_banner -i "$1" \
    -vf "v360=input=equirect:output=rectilinear:d_fov=$FOV:w=$((W * 16)):h=$((H * 16)):yaw=$Y:pitch=$P:roll=$R" \
    "$N.png"
  convert "$N.png" -colorspace RGB -geometry "${W}x${H}" -colorspace sRGB "$N.ppm"
done
seq 10000 11023 |
sed s/$/.ppm/g |
paste - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
cat -n |
while read n cols
do
  pnmcat -lr $cols > row-$n.ppm
  echo row-$n.ppm
done |
paste - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
cat -n |
while read n row
do
  pnmcat -tb $row > OUT-$n.ppm
  echo OUT-$n.ppm
done
