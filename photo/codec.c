#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// window size
#define WINW (1<<10)
#define WINH (1<<10)

// export PPM stream on stdout
#define save_ppm 0

// skip swapbuffers for faster output
#define turbo 0

// input image must be square power of two, this is log2 of the size
#define N 6 // search NCHANGE for other places that need changing

// size of input image
#define W (1<<N)
#define H (1<<N)

// number of channels in the image
// currently some of the code is hardcoded to C=3 and needs updating...
#define C 3

// number of levels to skip from the top (e.g. for tiled input images)
// must be between 0 and (N-2), inclusive
#define SKIP_TOP 0

// number of levels to skip from the bottom (makes output image more imagey)
// must be between 1 and (N-1), inclusive
#define SKIP_BOTTOM 3

// side length of blocks to match (must be a power of two)
// currently some of the code assumes Q=2 and needs updating...
#define Q 2

// which geometric transformations to use
// currently 1, 2, 4 all work
// TRANSFORMS=8 is buggy and weird / broken and needs fixing...
#define TRANSFORMS 4

// whether to allow inverse video
#define negative 1

// limit for brightness scaling (between 0 and 1 guarantess contractiveness)
// turns out the PIFS is (usually?) eventually contractive,
// even with brightness=(1.0/0.0), though I have no proof...
#define brightness 1.0

// // // // // // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // // // // // //
// // end of configuration section, apart from lines marked NCHANGE below
// // // // // // // // // // // // // // // // // // // // // // // // //
// // // // // // // // // // // // // // // // // // // // // // // // //

// input image buffer
unsigned char in[H][W][C];

// number of cells
// [sum [ 4^n | n <- [0 .. m-1] ] | m <- [0..15]]
// [0,1,5,21,85,341,1365,5461,21845,87381,349525,1398101,5592405,22369621,89478485,357913941]
#if N == 11 // takes around 4.5 mins to compress/encode on AMD 2700X CPU
#define M 1398101
#else
#if N == 10
#define M 349525
#else
#if N == 9
#define M 87381
#else
#if N == 8
#define M 21845
#else
#if N == 7
#define M 5461
#else
#if N == 6
#define M 1365
#else
#error unsupported N
#endif
#endif
#endif
#endif
#endif
#endif

// quad tree with mipmaps that waste most of the space
float tree[2/*1+log2Q*/][M][Q][Q][C];
float stddev[M];

// output image buffer
//unsigned char out[H/Q*H][W/Q*W][C];
unsigned char out[WINH][WINW][C];

// input buffer with mipmaps that waste most of the space
float input[N][H][W][C];

// encoded image (back links from bottom level)
float A[H/Q][W/Q][C]; // added to destination block
float B[H/Q][W/Q][C]; // multiplied by source block and added to destination block
int IX[H/Q][W/Q]; // source block index

struct backlink
{
  int kjio[4];
  float a[4];
  float b[4];
};

struct backlink graph[H/Q][W/Q];

int ix(int k, int j, int i)
{
  int o = 0, w = W/Q, n = k - 1/*FIXME log2(Q)*/;
  assert(n >= 0);
  while (n > 0)
  {
    o += w * w;
    w /= 2;
    n -= 1;
  }
  return o + w * j + i;
}

int compose[8][8];
int inverted[8] = { 0, 1, 2, 3, 4, 6, 5, 7 };

void mkcompose(void)
{
  const int MM[8][2][2] =
    { {{1,0},{0,1}}, {{1,0},{0,-1}}, {{-1,0},{0,1}}, {{-1,0},{0,-1}}
    , {{0,1},{1,0}}, {{0,1},{-1,0}}, {{0,-1},{1,0}}, {{0,-1},{-1,0}}
    };
  for (int a = 0; a < 8; ++a)
  for (int b = 0; b < 8; ++b)
  {
    int m[2][2] = {{0,0},{0,0}};
    for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j)
    for (int k = 0; k < 2; ++k)
      m[i][k] += MM[a][i][j] * MM[b][j][k];
    for (int c = 0; c < 8; ++c)
      if (m[0][0] == MM[c][0][0] && m[0][1] == MM[c][0][1] &&
          m[1][0] == MM[c][1][0] && m[1][1] == MM[c][1][1])
        compose[a][b] = c;
  }
}

void child(struct backlink *e0, float *px0, float *py0)
{
  // copy
  struct backlink e = *e0;
  float px = *px0;
  float py = *py0;
  // calculate
  assert(e.kjio[0] > 1);
  assert(e.kjio[0] <= N);
  assert(e.kjio[1] >= 0);
  assert(e.kjio[1] < (H >> e.kjio[0]));
  assert(e.kjio[2] >= 0);
  assert(e.kjio[2] < (W >> e.kjio[0]));
  assert(e.kjio[3] >= 0);
  assert(e.kjio[3] < 8);
  e.kjio[1] *= 2;
  e.kjio[2] *= 2;
  if (py >= 0.5) e.kjio[1] += 1;
  if (px >= 0.5) e.kjio[2] += 1;
  e.kjio[0] -= 1;
  px *= 2.0;
  py *= 2.0;
  px -= floorf(px);
  py -= floorf(py);
  // restore
  if (e.kjio[0] == 1) /* log2 q */
  {
    assert(0 <= e.kjio[1]);
    assert(e.kjio[1] < H/Q);
    assert(0 <= e.kjio[2]);
    assert(e.kjio[2] < W/Q);
    struct backlink f = graph[e.kjio[1]][e.kjio[2]];
    float X[8] = { px, px, 1 - px, 1 - px, py, py, 1 - py, 1 - py };
    float Y[8] = { py, 1 - py, py, 1 - py, px, 1 - px, px, 1 - px };
    f.kjio[3] = compose[f.kjio[3]][e.kjio[3]];
    px = X[f.kjio[3]];
    py = Y[f.kjio[3]];
    e = f;
  }
  assert(e.kjio[0] > 1);

  *e0 = e;
  *px0 = px;
  *py0 = py;
}

int O[H/Q][W/Q]; // orientation transformation index

// simple linear regression
// y = a + b * x
void linear(float *a, float *b, const float *x, const float *y, int n)
{
  float sx = 0, sx2 = 0, sy = 0, sy2 = 0, sxy = 0;
  for (int i = 0; i < n; ++i)
  {
    sx += x[i];
    sx2 += x[i] * x[i];
    sy += y[i];
    sy2 += y[i] * y[i];
    sxy += x[i] * y[i];
  }
  *a = (sy * sx2 - sx * sxy) / (n * sx2 - sx * sx);
  *b = (n * sxy - sx * sy) / (n * sx2 - sx * sx);
}

// rms error metric
// don't actually do the root-mean step, as only ordering matters later
float rms(float a, float b, const float *x, const float *y, int n)
{
  float e = 0;
  for (int i = 0; i < n; ++i)
  {
    float d = a + b * x[i] - y[i];
    e += d * d;
  }
  return e; // sqrtf(e / n);
}

// encode into partitioned iterated function system
void compress(void)
{
  // for each block in the base image
  #pragma omp parallel for collapse(2)
  for (int v = 0; v < H/Q; ++v)
  for (int u = 0; u < W/Q; ++u)
  {
    float needle[C][Q][Q];
    for (int j = 0; j < Q; ++j)
    for (int i = 0; i < Q; ++i)
    for (int c = 0; c < C; ++c)
      needle[c][j][i] = input[0][Q*v+j][Q*u+i][c];
    float e_min = 1.0f/0.0f;

    // for each orientation of each block in the rest of the quad tree
    for (int n = SKIP_BOTTOM; n < N - SKIP_TOP; ++n)
    for (int t = 0; t < (H>>n)/Q; ++t)
    for (int s = 0; s < (W>>n)/Q; ++s)
    for (int o = 0; o < TRANSFORMS; ++o)
    {
      float haystack[C][Q][Q];
      for (int j = 0; j < Q; ++j)
      for (int i = 0; i < Q; ++i)
      for (int c = 0; c < C; ++c)
      {
        int X[8] = { i, i, Q-1-i, Q-1-i, j, j, Q-1-j, Q-1-j };
        int Y[8] = { j, Q-1-j, j, Q-1-j, i, Q-1-i, i, Q-1-i };
        haystack[c][j][i] = input[n][Q*t+X[o]][Q*s+Y[o]][c];
      }

      // compute the block transformation
      float a[C], b[C], e = 0;
      for (int c = 0; c < C; ++c)
      {
        linear(&a[c], &b[c], &haystack[c][0][0], &needle[c][0][0], Q*Q);

        // ensure positive (avoid inverse video)
        if (! negative)
          if (! (b[c] > 0)) b[c] = 0;

        // clamp scale factor
        if (! (b[c] < brightness)) b[c] = brightness;
        if (! (b[c] >-brightness)) b[c] =-brightness;
      }

      // keep the best match
      for (int c = 0; c < C; ++c)
      {
        e += rms(a[c], b[c], &haystack[c][0][0], &needle[c][0][0], Q*Q);
      }
      if (e < e_min)
      {
        for (int c = 0; c < C; ++c)
        {
          A[v][u][c] = a[c];
          B[v][u][c] = b[c];
        }
        IX[v][u] = ix(n + 1, t, s);
        O[v][u] = o;
        e_min = e;
        // packed copy for opengl use
        graph[v][u].kjio[0] = n + 1 /* log2 Q */;
        graph[v][u].kjio[1] = t;
        graph[v][u].kjio[2] = s;
        graph[v][u].kjio[3] = o;
        for (int c = 0; c < C; ++c)
        {
          graph[v][u].a[c] = a[c];
          graph[v][u].b[c] = b[c];
        }
      }
    }
  }
}

// one step of decoding the iterated function system
void decompress(void)
{
  // back links with transformation
  #pragma omp parallel for collapse(4)
  for (int v = 0; v < H/Q; ++v)
  for (int u = 0; u < W/Q; ++u)
  for (int j = 0; j < Q; ++j)
  for (int i = 0; i < Q; ++i)
  {
    // FIXME assumes W=H
    int X[8] = { i, i, Q-1-i, Q-1-i, j, j, Q-1-j, Q-1-j };
    int Y[8] = { j, Q-1-j, j, Q-1-j, i, Q-1-i, i, Q-1-i };
    for (int c = 0; c < C; ++c)
      tree[0][ix(1 /* FIXME log2 Q */, v, u)][j][i][c] = tree[0][IX[v][u]][Y[O[v][u]]][X[O[v][u]]][c] * B[v][u][c] + A[v][u][c];
  }
  // links from quad tree
  for (int n = 2; n < N; ++n)
  {
    #pragma omp parallel for collapse(7)
    for (int v = 0; v < H>>n; ++v)
    for (int u = 0; u < W>>n; ++u)
    for (int t = 0; t < 2; ++t)
    for (int s = 0; s < 2; ++s)
    for (int j = 0; j < Q/2; ++j)
    for (int i = 0; i < Q/2; ++i)
    for (int c = 0; c < C; ++c)
      tree[0][ix(n, v, u)][Q/2*t+j][Q/2*s+i][c] = tree[1][ix(n-1, 2*v+t, 2*u+s)][j][i][c];
  }
  // generate mipmaps
  for (int n = 1; n <= 1/*log2 Q*/; ++n)
  #pragma omp parallel for collapse(4)
  for (int m = 0; m < M; ++m)
  for (int v = 0; v < Q>>n; ++v)
  for (int u = 0; u < Q>>n; ++u)
  for (int c = 0; c < C; ++c)
    tree[n][m][v][u][c] = 0.25f *
      ( tree[n-1][m][2*v+0][2*u+0][c]
      + tree[n-1][m][2*v+0][2*u+1][c]
      + tree[n-1][m][2*v+1][2*u+0][c]
      + tree[n-1][m][2*v+1][2*u+1][c]
      );
}

#define GLSL(s) #s

const char *vert = "#version 450 core\n" GLSL(

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 tc;

out vec2 p0;

layout(location = 0) uniform vec2 delta;
layout(location = 1) uniform float zoom;
layout(location = 5) uniform float aspect;

void main(void)
{
  gl_Position = vec4(1, -1, 1, 1) * vec4(vec2(1, aspect) * (zoom * (pos - delta) + delta), 0.0, 1.0);
  p0 = tc;
}

);

const char *frag = "#version 450 core\n" GLSL(

in vec2 p0;

out vec4 colour;

const int N = 6; // NCHANGE
const int depth = 11; // detail level; not worth going above log2(window size)

const int dim = 1 << N;

const int M[16] = int[16](0,1,5,21,85,341,1365,5461,21845,87381,349525,1398101,5592405,22369621,89478485,357913941);

struct backlink
{
  ivec4 kjio;
  vec4 a;
  vec4 b;
};

layout(binding = 0) uniform sampler2DArray tex;

layout(std430, binding = 1) buffer g
{
  // backlinks from lowest level of the quad tree to higher levels
  backlink graph[dim>>1][dim>>1];
};

layout(std430, binding = 2) buffer t
{
  // flat array instead of multidimensional
  // see: <https://gitlab.freedesktop.org/mesa/mesa/-/issues/11128>
  float tree[1*M[N]*2*2*3];
};

// texture array emulation using shader storage buffer array
vec4 lookup(vec2 p, int m)
{
  if (m < 0) m = M[N]-1;
  if (m >= M[N]) m = M[N]-1;
  int j = p.y >= 0.5 ? 1 : 0;
  int i = p.x >= 0.5 ? 1 : 0;
  vec4 o;
  for (int c = 0; c < 3; ++c)
    o[c] = tree[((m*2+j)*2+i)*3+c];
  o[3] = 1.0;
  return o;
}

layout(location = 1) uniform float zoom;
layout(location = 2) uniform ivec4 kjio0;
layout(location = 3) uniform vec4 a0;
layout(location = 4) uniform vec4 b0;

// get index into tree table
int index(backlink e)
{
  int o = 0;
  int w = dim/2;
  int n = e.kjio[0] - 1/*FIXME log2(Q)*/;
  while (n > 0)
  {
    o += w * w;
    w /= 2;
    n -= 1;
  }
  return o + w * e.kjio[1] + e.kjio[2];
}

// transformations for each orientation
const mat3x2 affine[8] = mat3x2[8]
  ( mat3x2( 1, 0,  0,  1,  0, 0)
  , mat3x2( 1, 0,  0, -1,  0, 1)
  , mat3x2(-1, 0,  0,  1,  1, 0)
  , mat3x2(-1, 0,  0, -1,  1, 1)
  , mat3x2( 0, 1,  1,  0,  0, 0)
  , mat3x2( 0, 1, -1,  0,  0, 1)
  , mat3x2( 0,-1,  1,  0,  1, 0)
  , mat3x2( 0,-1, -1,  0,  1, 1)
  );

// get child node of e at coordinates p
void child(inout backlink e, inout vec2 p)
{
  if (e.kjio[0] == 1)
  {
    // A + B (a + b X)
    vec4 a = e.a;
    vec4 b = e.b;
    e = graph[e.kjio[1] & (dim/2-1)][e.kjio[2] & (dim/2-1)];
    e.a = a + b * e.a;
    e.b = e.b * b;
    p = affine[e.kjio[3] & 7] * vec3(p, 1);
  }
  e.kjio[1] *= 2;
  e.kjio[2] *= 2;
  if (p.y >= 0.5) e.kjio[1] += 1;
  if (p.x >= 0.5) e.kjio[2] += 1;
  p *= 2.0;
  p -= floor(p);
  e.kjio[0] -= 1;
}

// get the colour of coordinates p of child node e
vec4 shade(backlink e, vec2 p)
{
  return vec4(vec3(e.a + e.b * lookup(p, index(e))), 1);
}

// colour a pixel
void main(void)
{
  vec2 p = p0;
  backlink e = backlink(kjio0, a0, b0);
  // transform from window space to tile space
  p = affine[e.kjio[3] & 7] * vec3(p, 1);
  // simple trapezioidal window function over zoom depth
  vec4 c = shade(e, p) * (1.0 - log2(zoom)); // fade out top level
  for (int k = 0; k < depth; ++k)
  {
    child(e, p);
    c += shade(e, p); // sum intermediate levels
  }
  child(e, p);
  c += shade(e, p) * (log2(zoom)); // fade in bottom level
  // convert from linear light to somewhat more like sRGB (gamma 2)
  colour = sqrt(c / c.a);
}

);

// shader compilation helpers

static void debug_program(GLuint program, const char *name) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);
    if (linked != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL shader program link failed\n", name);
    }
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, 0, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL shader program info log\n", name);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
    assert(linked == GL_TRUE);
  } else {
    fprintf(stderr, "%s: OpenGL shader program creation failed\n", name);
  }
}

static void debug_shader(GLuint shader, GLenum type, const char *name) {
  const char *tname = 0;
  switch (type) {
    case GL_VERTEX_SHADER:   tname = "vertex";   break;
    case GL_FRAGMENT_SHADER: tname = "fragment"; break;
    case GL_COMPUTE_SHADER:  tname = "compute";  break;
    default:                 tname = "unknown";  break;
  }
  if (shader) {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (compiled != GL_TRUE) {
      fprintf(stderr, "%s: OpenGL %s shader compile failed\n", name, tname);
    }
    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    char *buffer = (char *) malloc(length + 1);
    glGetShaderInfoLog(shader, length, 0, buffer);
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "%s: OpenGL %s shader info log\n", name, tname);
      fprintf(stderr, "%s\n", buffer);
    }
    free(buffer);
    assert(compiled == GL_TRUE);
  } else {
    fprintf(stderr, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

static void compile_shader(GLint program, GLenum type, const char *name, const GLchar *source) {
  GLuint shader = glCreateShader(type);
  glShaderSource(shader, 1, &source, 0);
  glCompileShader(shader);
  debug_shader(shader, type, name);
  glAttachShader(program, shader);
  glDeleteShader(shader);
}

static GLint compile_program(const char *name, const GLchar *vert, const GLchar *frag) {
  GLint program = glCreateProgram();
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , name, vert); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, name, frag); }
  glLinkProgram(program);
  debug_program(program, name);
  return program;
}

// main entry point

int main(int argc, char **argv)
{
  (void) argc;
  (void) argv;
  mkcompose();
#if 0
  for (int i = 0; i < 8; ++i)
  {
    for (int j = 0; j < 8; ++j)
    {
      fprintf(stderr, ", %d", compose[i][j]);
    }
    fprintf(stderr, "\n");
  }
#endif
  srand(time(0));

  // read the input (raw data with no header, size must match)
  fread(in, H*W*C, 1, stdin);

  // convert to linear float in mipmap base level
  // assumes gamma 2
  for (int j = 0; j < H; ++j)
  for (int i = 0; i < W; ++i)
  for (int c = 0; c < C; ++c)
    input[0][j][i][c] = (in[j][i][c]/255.0f * in[j][i][c]/255.0f);

  // generate input mipmaps
  for (int n = 1; n < N; ++n)
  for (int j = 0; j < H>>n; ++j)
  for (int i = 0; i < W>>n; ++i)
  for (int c = 0; c < C; ++c)
    input[n][j][i][c] = 0.25f *
      ( input[n-1][2*j+0][2*i+0][c]
      + input[n-1][2*j+0][2*i+1][c]
      + input[n-1][2*j+1][2*i+0][c]
      + input[n-1][2*j+1][2*i+1][c]
      );

  // encode
  fprintf(stderr, "compress...\n");
  compress();

  // analyse
  int fixed = 0;
  for (int v = 0; v < H/Q; ++v)
  for (int u = 0; u < W/Q; ++u)
    fixed += graph[v][u].kjio[0] == N;
  fprintf(stderr, "found %d fixed points...\n", fixed);

  // decode (usually after 10 passes it is visually indistinguishable)
  fprintf(stderr, "decompress...\n");
  for (int pass = 0; pass < 100; ++pass)
  {
    decompress();
  }
  #pragma omp parallel for
  for (int m = 0; m < M; ++m)
  {
    double s = 0;
    for (int c = 0; c < C; ++c)
    {
      double s0 = 0, s1 = 0, s2 = 0;
      for (int j = 0; j < Q; ++j)
      for (int i = 0; i < Q; ++i)
      {
        s0 += 1;
        s1 += tree[0][m][j][i][c];
        s2 += tree[0][m][j][i][c] * tree[0][m][j][i][c];
      }
      s += (s0 * s2 - s1 * s1) / (s0 * s0);
    }
    stddev[m] = (s);
  }

  // initialize window
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(WINW, WINH, "codec", 0, 0);
  glfwMakeContextCurrent(window);
  if (! window) {
    fprintf(stderr, "GLFW error (needs display with OpenGL 4.5)\n");
    return 1;
  }
  glewInit();

  // compile shader
  fprintf(stderr, "shader...\n");
  GLuint program = compile_program("shader", vert, frag);
  glUseProgram(program);

  // vertex array
  fprintf(stderr, "vertex array...\n");
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  // vertex buffer
  fprintf(stderr, "vertex buffer...\n");
  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  GLfloat vbo_data[] =
    { -1, -1, 0, 0
    , -1,  1, 0, 1
    ,  1, -1, 1, 0
    ,  1,  1, 1, 1
    };
  glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), vbo_data, GL_STATIC_DRAW);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), ((char *)0) + 2 * sizeof(GLfloat));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  // shader storage buffer
  fprintf(stderr, "graph buffer...\n");
  GLuint ssbo;
  glGenBuffers(1, &ssbo);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
  glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(graph), graph, GL_STATIC_DRAW);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ssbo);

  // texture array emulation via shader storage buffer (no layer limit)
  fprintf(stderr, "tree buffer...\n");
  GLuint tex;
  glGenBuffers(1, &tex);
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, tex);
  size_t bytes = sizeof(tree[0][0][0][0][0]) * 1 * M * Q * Q * C;
  fprintf(stderr, "texture bytes: %lld\n", (long long int) bytes);
  glBufferData(GL_SHADER_STORAGE_BUFFER, bytes, tree, GL_STATIC_DRAW);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, tex);

  fprintf(stderr, "ready!\n");

  // main loop
  float a0[4] = {0,0,0,0};
  float b0[4] = {1,1,1,1};
  float a1[4] = {0,0,0,0};
  float b1[4] = {1,1,1,1};
  struct backlink e = { { N, 0, 0, 0 }, { 0.0f, 0.0f, 0.0f, 0.0f }, { 1.0f, 1.0f, 1.0f, 1.0f } };
  float dx = (rand() & 1) * 0.5f + 0.25f;
  float dy = (rand() & 1) * 0.5f + 0.25f;
  float bdx = dx > 0.5f ? 1 : -1;
  float bdy = dy > 0.5f ? 1 : -1;
  double bpm = 124;
  double period = 2 * 60 * 60 / bpm;
  double phase = 0.0;

  for (int frame = 0; ; ++frame)
  {
    // report errors (there should be none)
    {
      int err;
      while ((err = glGetError())) fprintf(stderr, "GL ERROR %d\n", err);
    }

    // interactivity
    glfwPollEvents();
    if (glfwWindowShouldClose(window)) break;

    // handle zoom step state update
    while (phase >= period)
    {
      phase -= period;

      // switch to child node
      child(&e, &dx, &dy);

      // copy colour transformation 
      for (int c = 0; c < 4; ++c)
      {
        a0[c] = e.a[c];
        b0[c] = e.b[c];
      }

      // choose an interesting child
      struct backlink e00 = e, e01 = e, e10 = e, e11 = e;
      dx = 0.25; dy = 0.25; child(&e00, &dx, &dy);
      dx = 0.75; dy = 0.25; child(&e01, &dx, &dy);
      dx = 0.25; dy = 0.75; child(&e10, &dx, &dy);
      dx = 0.75; dy = 0.75; child(&e11, &dx, &dy);
      float s00 = stddev[ix(e00.kjio[0] + 1, e00.kjio[1], e00.kjio[2])];
      float s01 = stddev[ix(e01.kjio[0] + 1, e01.kjio[1], e01.kjio[2])];
      float s10 = stddev[ix(e10.kjio[0] + 1, e10.kjio[1], e10.kjio[2])];
      float s11 = stddev[ix(e11.kjio[0] + 1, e11.kjio[1], e11.kjio[2])];
      float s = s00 + s01 + s10 + s11;
      float coin = s * rand() / (double) RAND_MAX;
      int w = (coin < s00) + (coin < s00 + s01) + (coin < s00 + s01 + s10);
      //int w = rand() % 4;
      float X[4] = { 0.75, 0.25, 0.75, 0.25 };
      float Y[4] = { 0.75, 0.75, 0.25, 0.25 };
      dx = X[w];
      dy = Y[w];

      // convert to screen coordinates
      bdx = dx > 0.5 ? 1 : -1;//dx * 4 - 2;
      bdy = dy > 0.5 ? 1 : -1;//dy * 4 - 2;
      {
        float X[8] = { bdx, bdx,  - bdx,  - bdx, bdy, bdy,  - bdy,  - bdy };
        float Y[8] = { bdy,  - bdy, bdy,  - bdy, bdx,  - bdx, bdx,  - bdx };
        bdx = X[inverted[e.kjio[3]]];
        bdy = Y[inverted[e.kjio[3]]];
      }
    }

    // zoom and colour transformation blending
    float t = phase / period;
    float zoom = powf(2.0f, t);
    for (int c = 0; c < 4; ++c)
    {
      e.a[c] = a0[c] + t * (a1[c] - a0[c]);
      e.b[c] = b0[c] + t * (b1[c] - b0[c]);
    }

    // draw
    glUniform2f(0, bdx, bdy);
    glUniform1f(1, zoom);
    glUniform4iv(2, 1, &e.kjio[0]);
    glUniform4fv(3, 1, &e.a[0]);
    glUniform4fv(4, 1, &e.b[0]);
    glUniform1f(5, WINW / (float) WINH);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    phase += 1.0;

    // save PPM stream to stdout
    if (save_ppm)
    {
      glReadPixels(0, 0, WINW, WINH, GL_RGB, GL_UNSIGNED_BYTE, &out[0][0][0]);
      printf("P6\n%d %d\n255\n", WINW, WINH);
      for (int j = WINH - 1; j >= 0; --j)
      {
        fwrite(&out[j][0][0], WINW * 3, 1, stdout);
      }
      fflush(stdout);
    }

    // display
    if (! turbo)
    {
      glfwSwapBuffers(window);
    }
  }

  return 0;
}
