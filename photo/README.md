# codec

fractal image compressor/decompressor with zooming visualization

## usage

```
make &&
pngtopnm < cactus-64x64.png |
tail -c $((64*64*3)) |
./codec
```

# fractal-photo-mosaic

render a zooming fractal video from a collection of images

## usage

```
mkdir images
cd images
wget *.jpeg *.png # needs exactly 103 images total, TODO FIXME hardcoding
../prepare.sh
cd ..
ln -s images/image.data
ln -s images/thumbs.data
make
./fractal-photo-mosaic |
ffmpeg -i soundtrack.wav -f image2pipe -codec ppm -framerate 60 -i - \
  -pix_fmt yuv420p -profile:v high -level:v 4.1 -b:v 20M -b:a 192k \
  fractal-photo-mosaic.mkv
```
